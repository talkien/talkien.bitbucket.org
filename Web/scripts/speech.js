var recordAnswer = function($scope, callback){
    var correct_answer = $scope.current_card.title.toLowerCase();

    var rec = new webkitSpeechRecognition();
    var finalized = false;
    rec.continuous = true;
    rec.interimResults = true;
    rec.lang = "en-GB";

    rec.onresult = function(e) {
        var results = e.results[e.results.length - 1];
        var transcription = results[0].transcript.toLowerCase();
        console.log(transcription);

        if (transcription.indexOf(correct_answer) >=0 && !finalized){
            finalized = true;
            rec.stop();
            callback($scope);
        }
    };
    rec.onstart = function(e) {
        console.log('start')
    };
    rec.onerror = function(e) {
        console.log(e);
    };
    rec.onend = function(e) {
        console.log('end')
    };
    rec.onspeechstart = function(e) {
        console.log('speechStart')
    };
    rec.start();
}
