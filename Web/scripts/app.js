(function() {
    var app = angular.module('game', [
        'ngRoute'
    ]);
    // ====================================================================
    // Data collection functions
    // ====================================================================
    var getCards = function($scope, $http, callback) {
        $http.get('/data.json').success(function(data) {
            console.log(data);
            if (data.length > 0) {
                $scope.cards = data;
                callback();
            } else {
                alert('No cards are found');
            }
        });
    }
    // ====================================================================
    // Controllers
    // ====================================================================
    app.controller('GameHome', ['$scope', '$http',
        function($scope, $http) {

        }
    ]);
    app.controller('GamePlay', ['$scope', '$http', '$routeParams',
        function($scope, $http, $routeParams) {
            var current_index = $routeParams.current_index;

            getCards($scope, $http, function() {
                console.log('current index ' + current_index);
                $scope.current_card = $scope.cards[current_index];

                if (current_index < $scope.cards.length) {
                    recordAnswer($scope, function() {
                        current_index++;
                        window.location = "#/gameplay/" + current_index;
                    });
                } else {
                    window.location = "#/";
                }
            });

        }
    ]);

    // ====================================================================
    // Routers
    // ====================================================================
    app.config(['$routeProvider',
        function($routeProvider) {
            $routeProvider.
            when('/', {
                templateUrl: 'views/home.html',
                controller: 'GameHome'
            }).
            when('/gameplay/:current_index', {
                templateUrl: 'views/game/card.html',
                controller: 'GamePlay'
            }).
            when('/gameplay/results', {
                templateUrl: 'views/game/card.html',
                controller: 'GameResults'
            }).
            otherwise({
                redirectTo: '/'
            });
        }
    ]);

})();
