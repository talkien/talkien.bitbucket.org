var express = require('express');
var logger = require('./logger');
var app = express();

// Catching errors Opbeat.com
var opbeat = require('opbeat')({
    organizationId: process.env.OPBEAT_ORGANIZATION_ID || 'c2290a42d12a49a4afe6a3895dc914e3',
    appId: process.env.OPBEAT_APP_ID || 'd97d741733',
    secretToken: process.env.OPBEAT_SECRET_TOKEN || 'f2453da3bdd836604d96fd953d6d213b9dcdd284'
});


app.use(logger);
app.use(express.static('./Web'));

var port = process.env.PORT || 3000;
app.listen(port, function() {
    console.log('Listening on '+port);
});
